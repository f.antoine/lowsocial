import { Component } from '@angular/core';

import { Author } from '../mocks/author';
import { AUTHORS } from '../mocks/mock-author';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  constructor() { }

  authors: Author[] = null;

  ngOnInit() {
    this.authors = AUTHORS;
  }

}
