import { Component, OnInit } from '@angular/core';
import { ModalController, ToastController } from '@ionic/angular';

import { Message } from '../mocks/messages';
import { MESSAGES } from '../mocks/mock-message';

import { Author } from '../mocks/author';
import { AUTHORS } from '../mocks/mock-author';

@Component({
  selector: 'app-create-message',
  templateUrl: './create-message.page.html',
  styleUrls: ['./create-message.page.scss'],
})
export class CreateMessagePage implements OnInit {

  constructor(private modalCtrl: ModalController, public toastController: ToastController) { }

  messages: Message[] = null;
  authors: Author[] = null;

  newMessage: string;

  ngOnInit() {
    this.messages = MESSAGES;
    this.authors = AUTHORS;
  }

  closeModal() {
    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }

  async presentToast() {
    const toast = await this.toastController.create({
      message: 'Your message is empty',
      duration: 1000
    });
    toast.present();
  }

  submit(author: Author) {
    if (!this.newMessage) {
      this.presentToast()
    } else {
      this.messages.unshift({
        img: author.img,
        author: author.name,
        text: this.newMessage
      });
      this.closeModal();
      this.messages = MESSAGES;
    }
  }

}
