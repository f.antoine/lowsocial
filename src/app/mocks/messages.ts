export class Message {
    img: string;
    author: string;
    text: string;
}