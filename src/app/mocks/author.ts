export class Author {
    img: string;
    name: string;
    tel: number;
    age: number;
    city: string;
    sex: string;
    fact: string
}