import { Author } from './author';

export const AUTHORS: Author[] = [
    {
        img: "https://66.media.tumblr.com/ed99816e837808d7ce30a1b4d0976c9c/tumblr_inline_o8xkg4dwFq1sss5ih_540.png",
        name: "John Doe",
        tel: 123456789,
        age: 40,
        city: "New York",
        sex: "Male",
        fact: "Recuperating from a broken leg, adventuresome professional photographer L. B. Jeff Jefferies (Stewart) is confined to a wheelchair in his Greenwich Village apartment. His rear window looks out onto a courtyard and several other apartments. During a powerful heat wave, he watches his neighbors, who keep their windows open to stay cool."
    }
]