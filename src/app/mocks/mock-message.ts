import { Message } from './messages';

export const MESSAGES: Message[] = [
    {
        img: "https://www.humanafterhal.com/wp-content/uploads/arthur-charles-clarke-auteur-science-fiction.jpg",
        author: "Arthur C. Clarke",
        text: "Has autem provincias, quas Orontes ambiens amnis imosque"
    },
    {
        img: "https://www.humanafterhal.com/wp-content/uploads/photo-philipkdick.jpg",
        author: "Philip K. Dick",
        text: "Aenean ac est sed sapien pulvinar dapibus ac posuere arcu. Etiam ultrices felis lectus, sed accumsan nisl faucibus ac. In purus est, tincidunt cursus magna non, commodo accumsan eros. "
    },
    {
        img: "https://www.bbvaopenmind.com/wp-content/uploads/2018/02/Sagan-1.jpg",
        author: "Carl Sagan",
        text: "Integer ullamcorper augue ac mollis finibus. Phasellus sit amet lectus sed dolor consectetur pharetra vel eu ligula. Phasellus ultricies turpis at orci cursus sodales."
    }
]