import { Component } from '@angular/core';

import { ModalController } from '@ionic/angular';
import { CreateMessagePage } from '../create-message/create-message.page';

import { Message } from '../mocks/messages';
import { MESSAGES } from '../mocks/mock-message';
import { from } from 'rxjs';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  messages: Message[] = null;

  constructor(public modalController: ModalController) { }

  ngOnInit() {
    this.messages = MESSAGES;
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: CreateMessagePage
    });
    return await modal.present();
  }

}
